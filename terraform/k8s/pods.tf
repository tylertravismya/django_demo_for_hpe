resource "kubernetes_replication_controller" "app-master" {
  metadata {
    name = "app-master"

    labels {
      app  = "djangodemo"
    }
  }

  spec {
    replicas = 1

    selector = {
      app  = "djangodemo"
    }
    template {        
      container {
        image = "realmethods/djangodemo:latest"
        name  = "app-container"
        
        port {
          container_port = 8080
        }
      }
    }
  }
}