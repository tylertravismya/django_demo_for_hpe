from django.contrib import admin

# Register your models here.
from .models.Company import Company
from .models.Employee import Employee
from .models.Address import Address

# Need to add this for each model that requires managing

admin.site.register(Company)
admin.site.register(Employee)
admin.site.register(Address)
