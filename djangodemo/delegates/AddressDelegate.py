from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Address import Address
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Address
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class AddressDelegate Declaration
#======================================================================
class AddressDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, addressId ):
		try:	
			address = Address.objects.filter(id=addressId)
			return address.first();
		except Address.DoesNotExist:
			raise ProcessingError("Address with id " + str(addressId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, address):
		for model in serializers.deserialize("json", address):
			model.save()
			return model;

	def create(self, address):
		address.save()
		return address;

	def saveFromJson(self, address):
		for model in serializers.deserialize("json", address):
			model.save()
			return address;
	
	def save(self, address):
		address.save()
		return address;
	
	def delete(self, addressId ):
		errMsg = "Failed to delete Address from db using id " + str(addressId)
		
		try:
			address = Address.objects.get(id=addressId)
			address.delete()
			return True
		except Address.DoesNotExist:
			raise ProcessingError("Address with id " + str(addressId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Address.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Address from db")
		except Exception:
			return None;
		
