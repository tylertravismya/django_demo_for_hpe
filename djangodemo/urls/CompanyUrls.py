from django.urls import path
from djangodemo.views import CompanyView

urlpatterns = [
    path('', CompanyView.index, name='index'),
	path('create', CompanyView.get, name='create'),
	path('get/<int:companyId>/', CompanyView.get, name='get'),
	path('save', CompanyView.save, name='save'),
	path('getAll', CompanyView.getAll, name='getAll'),
	path('delete/<int:companyId>/', CompanyView.delete, name='delete'),
	path('assignAddress/<int:companyId>/<int:AddressId>/', CompanyView.assignAddress, name='assignAddress'),
	path('unassignAddress/<int:companyId>/', CompanyView.unassignAddress, name='unassignAddress'),
	path('addEmployees/<int:companyId>/<EmployeesIds>/', CompanyView.addEmployees, name='addEmployees'),
	path('removeEmployees/<int:companyId>/<EmployeesIds>/', CompanyView.removeEmployees, name='removeEmployees'),
]
