from django.urls import path
from djangodemo.views import AddressView

urlpatterns = [
    path('', AddressView.index, name='index'),
	path('create', AddressView.get, name='create'),
	path('get/<int:addressId>/', AddressView.get, name='get'),
	path('save', AddressView.save, name='save'),
	path('getAll', AddressView.getAll, name='getAll'),
	path('delete/<int:addressId>/', AddressView.delete, name='delete'),
]
