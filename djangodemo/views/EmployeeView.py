import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.EmployeeDelegate import EmployeeDelegate

 #======================================================================
# 
# Encapsulates data for View Employee
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class EmployeeView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Employee index.")

def get(request, employeeId ):
	delegate = EmployeeDelegate()
	responseData = delegate.get( employeeId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	employee = json.loads(request.body)
	delegate = EmployeeDelegate()
	responseData = delegate.createFromJson( employee )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	employee = json.loads(request.body)
	delegate = EmployeeDelegate()
	responseData = delegate.save( employee )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, employeeId ):
	delegate = EmployeeDelegate()
	responseData = delegate.delete( employeeId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = EmployeeDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

