import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Employee import Employee
from djangodemo.delegates.EmployeeDelegate import EmployeeDelegate

 #======================================================================
# 
# Encapsulates data for model Employee
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class EmployeeTest Declaration
#======================================================================
class EmployeeTest (TestCase) :
	def test_crud(self) :
		employee = Employee()
		employee.firstName = "default firstName field value"
		employee.lastName = "default lastName field value"
		
		delegate = EmployeeDelegate()
		responseObj = delegate.create(employee)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


